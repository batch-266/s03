package com.zuitt.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        //Exceptions
        //A problem that arises during the execution of the program
        //It disrupts the normal flow of the program and terminates abnormally

        //Exception Handling
        //refers to managing and catching run-time errors in order to safely run your code.

        Scanner input = new Scanner(System.in);

        int num1 = 0;
        int num2 = 0;


        //try-catch-finally
        try {
            System.out.println("Enter a number: ");
            num1 = input.nextInt();
            System.out.println("Enter a number: ");
            num2 = input.nextInt();
            System.out.println("The qoutient is: " + num1/num2);
        } catch (ArithmeticException e) {
            System.out.println("Invalid Input!");
            e.printStackTrace();
        } catch (InputMismatchException e) {
            System.out.println("Please input numbers only.");
        } catch (Exception e){
            System.out.println("Something went wrong. Please try again!");
        } finally {
            System.out.println("This will exceute no matter what!");
        }
    }
}
