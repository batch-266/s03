import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num = 1;

        try{

            int factorial = 1;
            System.out.println("Please enter a number to compute its factorial!: ");
            num = input.nextInt();
            if (num <= 0){
                System.out.println("Please input a postive integer");
            } else {
                for (int i = 1; i <= num; i++){
                    factorial *= i;
                }
                System.out.println("The factorial of " + num + " is " + factorial);
            }

        } catch (InputMismatchException e){
            System.out.println("Invalid input! Please input a number.");

        }
        catch (Exception e){
            System.out.println("Something went wrong. Please try again!");
        }
    }
}
